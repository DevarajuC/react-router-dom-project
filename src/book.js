import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Book extends Component {
  render() {
    let path = this.props.match.path;
    path += "/page";
    return (
      <div>
        <h1> Book page!</h1>
        <ul>
          <li>1111</li>
          <li>2222</li>
          <li>3333</li>
          <li>4444</li>
          <li>
            <Link to={path}>Page</Link>
          </li>
        </ul>
      </div>
    );
  }
}
