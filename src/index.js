import React, { Component } from "react";
import ReactDOM from "react-dom";
import App from "./app";
import "bootstrap/dist/css/bootstrap.css";

class Index extends Component {
  state = {
    value: new Date().toLocaleTimeString(),
  };
  render() {
    return <App log={this.state.value} />;
  }
}
ReactDOM.render(<Index />, document.getElementById("root"));
