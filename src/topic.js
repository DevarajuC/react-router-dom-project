import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Topic extends Component {
  render() {
    let path = this.props.match.path;
    path += "/book";
    return (
      <div>
        <h1>Topic page!</h1>
        <ul>
          <li>aaaa</li>
          <li>bbbb</li>
          <li>cccc</li>
          <li>dddd</li>
          <li>
            <Link to={path}>Book</Link>
          </li>
        </ul>
      </div>
    );
  }
}
