import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Counter from "./counter";
import Topic from "./topic";
import Book from "./book";
import Page from "./page";

const Home = () => {
  return (
    <div>
      <h1>Home page!</h1>
      <ul>
        <li>
          <Link to="/counter">Goto Counter</Link>
        </li>
        <li>
          <Link to="/topic">Goto Topic</Link>
        </li>
      </ul>
    </div>
  );
};

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <h4>{this.props.log}</h4>
          <Route exact path="/" component={Home} />
          <Route exact path="/counter" component={Counter} />
          <Route exact path="/topic" component={Topic} />
          <Route exact path="/topic/book" component={Book} />
          <Route exact path="/topic/book/page" component={Page} />
        </div>
      </Router>
    );
  }
}
export default App;
